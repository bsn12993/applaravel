$(function(){
    loadRoles();
});

function loadRoles(){

    $.ajax({
        url:'/getRoles',
        type:'GET',
        dataType:'json',
        data:{},
        success:function(data){
            console.log(data);
            if(data.issuccess){
                $.each(data.result,function(k,v){
                    $("#rolId").append("<option value='"+v.id+"'>"+v.name+"</option>")
                });
            }
        },
        error:function(error){
            console.log(error);
        }
    });
}
