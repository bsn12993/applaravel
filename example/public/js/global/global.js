var url="http://localhost:8000/";
var token=$("meta[name='csrf-token']").attr('content');



function loadCategories(){
    $.ajax({
        url:'/listingCategories',
        type:'GET',
        headers:{'X-CSRF-TOKEN':token},
        dataType:'json',
        data:{},
        success:function(data){
            console.log(data);
            if(data.issuccess){
                $.each(data.result,function(k,v){
                    $("#categoryId").append('<option value='+v.id+'>'+v.name+'</option>')
                });
            }
        },
        error:function(msg){

        },
        complete:function(){

        }
    });
}
