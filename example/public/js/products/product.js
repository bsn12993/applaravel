var jsonProduct={
    id:0,
    name:'',
    description:'',
    price:'',
    status:0
};

var httprequest={
    GET:'GET',
    POST:'POST',
    PUT:'PUT',
    DELETE:'DELETE'
};

$(function(){
    loadProducts();
})

function loadProducts(){
    requestGet(url+"getProducts",'GET',{},token);
}

$("#form_createProduct").on('submit',function(evt){
    evt.preventDefault();
    jsonProduct.name=$("#name").val();
    jsonProduct.description=$("#description").val();
    jsonProduct.price=$("#price").val();
    jsonProduct.status=$("#status").val();
    request(url+"product","POST",jsonProduct,token);
});

$("#formUpdateProduct").on('submit',function(evt){
    evt.preventDefault();
    jsonProduct.id=$("#idproduct").val();
    jsonProduct.name=$("#name").val();
    jsonProduct.description=$("#description").val();
    jsonProduct.price=$("#price").val();
    jsonProduct.status=$("#status").val();
    
    request(url+"product/"+jsonProduct.id,'PUT',jsonProduct,token);
})

function requestGet(url,method,data,token){
    $.ajax({
        url:url,
        type:method,
        headers:{'X-CSRF-TOKEN':token},
        dataType:'json',
        data:data,
        success:function(data){
            if(data.issuccess){
                if(data.result.length>0){
                    $("#tbl_product").html("");
                    $.each(data.result,function(k,v){
                        $("#tbl_product").append(
                        "<tr>"+
                            "<td>"+v.name+"</td>"+
                            "<td>"+v.price+"</td>"+
                            "<td><a href='/product/"+v.id+"/edit' class='btn btn-primary'>Editar</a></td>"+
                            "<td><button type='button' class='btn btn-danger' onclick='deleteProduct("+v.id+")'>Eliminar</button></td>"+
                        "</tr>");
                    });
                }
            }
        },
        error:function(x,y,z){
            console.log(x.responseJSON.errors);
        },
        complete:function(){

        }
    })
}


function request(url,method,data,token){
    $.ajax({
        url:url,
        type:method,
        headers:{'X-CSRF-TOKEN':token},
        dataType:'json',
        data:data,
        beforeSend:function(){
            $("#msg-error").html("");
        },
        success:function(data){
            alert(data.message);
            if(method=='DELETE'){
                loadProducts();
            }
        },
        error:function(x,y,z){
            console.log(x.responseJSON.errors);
            if(x.responseJSON.errors.name!=undefined){
                $("#msg-error").append(x.responseJSON.errors.name+"</br>");
            }
            if(x.responseJSON.errors.description!=undefined){
                $("#msg-error").append(x.responseJSON.errors.description+"</br>");
            }
            if(x.responseJSON.errors.price!=undefined){
                $("#msg-error").append(x.responseJSON.errors.price+"</br>");
            }
        },
        complete:function(){

        }
    })
}

function deleteProduct(id){
    request(url+"product/"+id,'DELETE',{},token);
}