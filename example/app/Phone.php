<?php

namespace App;

class Phone
{
    public $office_phoneId;
    public $office_number;
    public $home_phoneId;
    public $home_number;

    public function __construct()
    {
        $this->office_phoneId=0;
        $this->office_number="";
        $this->home_phoneId=0;
        $this->home_number="";
    }
}