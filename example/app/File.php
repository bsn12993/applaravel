<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class File extends Model
{
    const TABLE         = 'files';
    const ID            ='id';
    const NAME          = 'name';
    const PATH          = 'path';
    const CREATED_AT    = 'created_at';
    const UPDATED_AT    = 'updated_at';


    protected $table=self::TABLE;
    public $timestamps=false;
    public $fillable=array(
        self::ID,
        self::NAME,
        self::PATH,
        self::CREATED_AT,
        self::UPDATED_AT
    );

    public function setPath($path){
        $this->attributes[self::PATH]=Carbon::now()->second.$path->getClientOriginalName();
        $name=Carbon::now()->second.$path->getClientOriginalName();
        \Storage::disk('local')->put($name,\File::get($path));
    }
}
