<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //protected $table    = 'products';
     
    const TABLE         = "products";
    const CLASS_NAME    = __CLASS__;
    const ID            = "id";
    const NAME          = "name";
    const DESCRIPTION   = 'description';
    const PRICE         = 'price';
    const STATUS        = 'status';
    const CREATED_AT    = 'created_at';
    const UPDATED_AT    = 'updated_at';

  

    protected $table    = self::TABLE;
    public $timestamps  = false;
    protected $fillable=array(
        self::NAME,
        self::DESCRIPTION,
        self::PRICE
    );
   
}
