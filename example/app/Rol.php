<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    const TABLE = 'roles';
    const CLASS_NAME=__CLASS__;
    const ID = 'id';
    const FK = 'rolId';
    const NAME = 'name';
    const CREATED_AT    = 'created_at';
    const UPDATED_AT    = 'updated_at';

    protected $table=self::TABLE;
    public $timestamps=false;
    protected $fillable=array(
        self::NAME
    );

}
