<?php

namespace App;

class UserMobile
{
    public $name;
    public $job;
    public $company_name;
    public $passwords;
    public $phones;
    public $emails;
    public $device;

    public function __construct()
    {
        $this->name="";
        $this->job="";
        $this->company_name="";
        $this->passwords=new Password();
        $this->phones=new Phone();
        $this->emails=new Email();
        $this->device=new Device();
    }

    public function setNameAttribute($v){
        $this->name=$v;
    }

    public function getNameAttribute(){
        return $this->name;
    }

}