<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Session;

class Admin
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth=$auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(empty($this->auth->user())){
            Session::flash('message-error','No esta autentificado');
            return redirect()->to('unauthorized');
        }
        if($this->auth->user()->rolId!=1){
            Session::flash('message-error','Sin privilegios. Solo administrador');
            return redirect()->to('unauthorized');
        }
        return $next($request);
    }
}
