<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;
use Mail;
use App\Http\Requests\MailRequest;

class MailController extends Controller
{
    public function index()
    {
        return view('mail.index');
    }

    public function store(MailRequest $request)
    {
        try{
            Mail::send('mail.template',$request->all(),function($msg){
                $msg->subject('Correo de Contacto');
                $msg->to('bsn_12903@hotmail.com');
            });
            Session::flash('message','Correo enviado correctamente');
            return Redirect::to('/mail');
        }
        catch(Exception $e){
            Session::flash('message',$e);
            return Redirect::to('/mail');
        }
    }
}
