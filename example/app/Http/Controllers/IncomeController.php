<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Income;
use Auth;
use Redirect;

class IncomeController extends Controller
{
    public function index()
    {
        $incomes=Income::all();
        return view('income.index',compact('incomes'));
    }

    public function create()
    {
        return view('income.create');
    }

    public function store(IncomeRequest $request)
    {
        $income=new Income;
        $income->date=$request->date;
        $income->amount=$request->amount;
        $income->userId=Auth::user()->id;
        $income->save();

        return redirect::to('/income');
    }

    public function edit($id)
    {
        $income=Income::find($id);
        return view('income.edit',['income'=>$income]);
    }

    public function update($id, IncomeRequest $request)
    {
        $income=Income::find($id);
        $income->date=$request->date;
        $income->amount=$request->amount;
        $income->save();
        return redirect::to('/income');
    }

    public function destroy()
    {

    }
}
