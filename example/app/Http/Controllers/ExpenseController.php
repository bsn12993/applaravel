<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expense;
use App\Http\Requests\ExpenseRequest;
use Auth;
use Redirect;

class ExpenseController extends Controller
{
    public function index()
    {
        $expenses=Expense::all();
        return view('expense.index',compact('expenses'));
    }

    public function create()
    {
        return view('expense.create');
    }

    public function store(ExpenseRequest $request)
    {
        
        $expense=new Expense; 
        $expense->date=$request->date;
        $expense->amount=$request->amount;
        $expense->categoryId=$request->categoryId;
        $expense->userId=Auth::user()->id;
        $expense->save();

        return redirect::to('/expense');
    }

    public function edit($id)
    {
        $expense=Expense::find($id);
        return view('expense.edit',['expense'=>$expense]);
    }

    public function update($id, ExpenseRequest $request)
    {
        $expense=Expense::find($id);
        $expense->date=$request->date;
        $expense->amount=$request->amount;
        $expense->categoryId=$request->categoryId;
        $expense->save();
        return redirect::to('/expense');
    }

    public function destroy($id)
    {
        
    }
}
