<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['only'=>'admin']);
    }

    public function index()
    {
        return view('front.index');
    }

    public function admin()
    {
        return view('admin');
    }

    public function user()
    {
        return view('user');
    }

    public function login()
    {
        return view('login');
    }
}
