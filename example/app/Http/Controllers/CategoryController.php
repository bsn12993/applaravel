<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Category;
use Session;
use Redirect;
use App\Http\Requests\CategoryRequest;

class CategoryController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index()
    {
        $categories=Category::all();
        return view('category.index',compact('categories'));
    }

    public function create()
    {
        return view('category.create');
    }

    public function store(CategoryRequest $request)
    {
        Category::create([
            'name'=>$request['name'],
            'description'=>$request['description']
        ]);
        return redirect::to('/category');
    }

    public function edit($id)
    {
        $category=Category::find($id);
        //return $category;
        return view('category.edit',['category'=>$category]);
    }

    public function update($id, CategoryRequest $request)
    {

    }

    public function destroy()
    {

    }

    public function listing()
    {
        $categories=Category::all();
        return response()->json([
            'issuccess'=>true,
            'message'=>'Se encontrarón categorias',
            'result'=>$categories
        ]);
    }

}
