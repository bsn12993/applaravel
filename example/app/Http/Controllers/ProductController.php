<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Http\Requests\ProductRequest;


class ProductController extends Controller
{
    public function index()
    {
        return view('products.index');
    }

    public function create()
    {
        return view('products.create');
    }

    public function store(ProductRequest $request)
    {
        if($request->ajax()){
            try{
                Product::create([
                    'name'=>$request['name'],
                    'description'=>$request['description'],
                    'price'=>$request['price'],
                    'status'=>$request['status']
                ]);
        
                return response()->json([
                    'issuccess'=>true,
                    'message'=>'Usuario creado correctamente',
                    'result'=>''
                ]);
            }
            catch(Exception $e){
                return response()->json([
                    'issuccess'=>false,
                    'message'=>'Usuario creado correctamente '.$e->getMessage(),
                    'result'=>''
                ]);
            }
        }
    }

    public function edit($id)
    {
        $product=Product::find($id);
        return view('products.edit',['product'=>$product]);
    }

    public function update($id, ProductRequest $request)
    {
        if($request->ajax()){
            try{
                $product=Product::find($id);
                $product->fill($request->all());
                $product->save();
    
                return response()->json([
                    'issuccess'=>true,
                    'message'=>'Usuario editado correctamente ',
                    'result'=>''
                ]);
            }
            catch(Exception $e){
                return response()->json([
                    'issuccess'=>false,
                    'message'=>$e->getMessage(),
                    'result'=>''
                ]);
            }
        }  
    }

    public function destroy($id)
    {
        try{
            $product=Product::find($id);
            $product->delete();
            return response()->json([
                'issuccess'=>true,
                'message'=>'Producto eliminado correctamente',
                'result'=>''
            ]);
        }
        catch(Exception $e){
            return response()->json([
                'issuccess'=>false,
                'message'=>$e->getMessage(),
                'result'=>''
            ]);
        }
    }

    /**
     * AJAX
     */

    public function listing()
    {
        $products=Product::all();
        if(count($products)>0){
            return response()->json([
                'issuccess'=>true,
                'message'=>'Se encontrarón datos de productos',
                'result'=>$products
            ]);
        }
        else{
            return response()->json([
                'issuccess'=>false,
                'message'=>'No se encontrarón datos de productos',
                'result'=>''
            ]);
        }
    }
}
