<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;
use Redirect;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use Validator;
use App\Services\IUserService;
use App\Services\UserService;

class UserController extends Controller
{
    private $user;
    public function __construct(IUserService $instance)
    {
        $this->user=$instance;
        //$this->middleware('admin',['only'=>['index','create','edit']]);
    }

    public function index()
    {
        //$users=User::all();
        $users = $this->user->findAll();
        return view('user.index',compact('users'));
    }

    public function create()
    {
        return view('user.create');
    }

    public function edit($id)
    {
        /*
        $user=User::find($id);
        $user->password="";
        */
        $user = $this->user->findById($id);
        return view('user.edit',['user'=>$user]);
    }

    public function store(UserCreateRequest $request)
    {

        $user=new User;
        $user->name=$request->name;
        $user->email=$request->email;
        $user->rolId=$request->rolId;
        $user->password=bcrypt($request['password']);
        $user->save();
        return redirect::to('/user');
    }

    public function update($id, UserCreateRequest $request)
    {
        $user=User::find($id);
        $user->name=$request->name;
        $user->email=$request->email;
        $user->rolId=$request->rolId;
        $user->password=bcrypt($request['password']);
        /*
        $user->fill([
            'name'=>$request['name'],
            'email'=>$request['email'],
            'password'=>bcrypt($request['password'])
        ]);
        */
        $user->save();
        Session::flash('message','Usuario editado correctamente');
        return redirect::to('/user');
    }

    public function destroy($id)
    {
        User::destroy($id);
        Session::flash('message','Usuario eliminado correctamente');
        return redirect::to('/user');
    }
}
