<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rol;
use Redirect;
use Session;
use App\Http\Requests\RolRequest;

class RolController extends Controller
{

    public function __construct()
    {
        //$this->middleware('admin',['only'=>['index','create','edit']]);
    }

    public function index()
    {
        $roles=Rol::all();
        return view("rol.index",compact('roles'));
    }

    public function create()
    {
        return view("rol.create");
    }

    public function store(RolRequest $request)
    {
        Rol::create([
            'name'=>$request['name']
        ]);
        Session::flash('message','Rol creado correctamente');
        return redirect::to('/rol');
    }

    public function edit($id)
    {
        $rol=Rol::find($id);
        return view("rol.edit",['rol'=>$rol]);
    }

    public function update($id, RolRequest $request)
    {
        $rol=Rol::find($id);
        $rol->fill($request->all());
        $rol->save();
        Session::flash('message','Rol editado correctamente');
        return redirect::to('/rol');
    }

    public function destroy($id)
    {
        Rol::destroy($id);
        Session::flash('message','Rol eliminado correctamente');
        return redirect::to('/rol');
    }

    public function listing()
    {
        $roles=Rol::all();
            return response()->json([
                'issuccess'=>true,
                'message'=>'Se recuperarón roles',
                'result'=>$roles
            ]);
    }
}
