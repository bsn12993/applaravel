<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\UserMobile;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $userMobile)
    {
        return response()->json($userMobile);
 
        /*
        $validator = Validator::make($userMobile->all(), [
            'name' => 'required',
            'job' => 'required',
            'company_name' => 'required',
            'passwords.password'=>'required',
            'passwords.password_confirm'=>'required',
            'phones.office_number'=>'required',
            'phones.home_number'=>'required',
            'emails.email'=>'required',
            'emails.notification_email'=>'required',
            'emails.emergency_email'=>'required',

            'device.token'=>'required',
            'device.platform'=>'required',
            'device.version'=>'required',
            'device.app_version'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        */
        return response()->json($userMobile);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
