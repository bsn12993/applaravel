<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use Redirect;
use App\Http\Requests\LoginRequest;

class LogController extends Controller
{

    public function index()
    {
        return view("login");
    }

    public function store(LoginRequest $request)
    {
        $credentials = [
            'email' => $request['email'],
            'password' => $request['password']
        ];
        
        if(Auth::attempt($credentials)){
            return Redirect::to("home");
        }
        Session::flash('message-error',"Datos son incorrectos");
        return Redirect::to('log');
 
    }

    public function unauthorized()
    {
        return view('unauthorized');
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::to('log');
    }
}
