<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    const TABLE = 'categories';
    const CLASS_NAME=__CLASS__;
    const ID = 'id';
    const FK = 'categoryId';
    const NAME = 'name';
    const DESCRIPTION = 'description';
    const CREATED_AT    = 'created_at';
    const UPDATED_AT    = 'updated_at';

    protected $table=self::TABLE;
    public $timestamps=false;
    protected $fillable=array(
        self::NAME
    );
}
