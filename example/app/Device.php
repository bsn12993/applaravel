<?php

namespace App;

class Device
{
    public $token;
    public $platform;
    public $version;
    public $app_version;

    public function __construct()
    {
        $this->token="";
        $this->platform="";
        $this->version="";
        $this->app_version="";
    }
}