<?php

namespace App\Services;

use App\Services\IUserServices;
use App\User;

class UserService implements IUserService
{
    public function findAll(){
        return User::all();
    }
    
    public function findById($id){
        $user=User::find($id);
        $user->password="";
        return $user;
    }

    public function create(Request $request){

    }

    public function update(Request $request){

    }

    public function delete(Request $request){
        
    }
}