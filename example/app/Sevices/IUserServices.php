<?php

namespace App\Services;

interface IUserService
{
    function findAll();
    function findById($id);
    function create(Request $request);
    function update(Request $request);
    function delete(Request $request);
}