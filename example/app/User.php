<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Rol;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    const TABLE = 'users';
    const CLASS_NAME=__CLASS__;
    const ID='id';
    const FK ='userId';
    const ROLE = Rol::FK;
    const NAME = 'name';
    const EMAIL = 'email';
    const FIRST_NAME='first_name';
    const LAST_NAME='last_name';
    const SECOND_NAME='second_last_name';
    const PHONE = 'phone';
    const PASSWORD = 'password';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const DELETED_AT='deleted_at';
    const STATUS = 'status';

    
    public function rol(){
        return $this->belongsTo(
            Rol::CLASS_NAME,
            self::ROLE
        );
    }

}
