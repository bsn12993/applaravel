<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Rol;

class Users extends Model
{
    const TABLE = 'users';
    const CLASS_NAME=__CLASS__;
    const ID='id';
    const FK ='userId';
    const ROLE = Rol::FK;
    const NAME = 'name';
    const EMAIL = 'email';
    const FIRST_NAME='first_name';
    const LAST_NAME='last_name';
    const SECOND_NAME='second_last_name';
    const PHONE = 'phone';
    const PASSWORD = 'password';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const DELETED_AT='deleted_at';
    const STATUS = 'status';

    use SoftDeletes;

    protected $table=self::TABLE;
    protected $fillable=array(
        self::NAME,
        self::EMAIL,
        self::PASSWORD
    );

    public function rol(){
        return $this->belongsTo(
            Rol::CLASS_NAME,
            self::ROLE
        );
    }

    public function setPasswordAttribute($pass)
    {
        $this->attributes[self::PASSWORD] =bcrypt($pass);
    }

    public function getAuthPassword(){
        return self::PASSWORD;
    }

}
