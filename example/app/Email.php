<?php

namespace App;

class Email
{
    public $same_email;
    public $email;
    public $notification_email;
    public $emergency_email;

    public function __construct()
    {
        $this->same_email=false;
        $this->email="";
        $this->notification_email="";
        $this->emergency_email="";
    }

}