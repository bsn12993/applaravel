<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    const TABLE = 'expenses';
    const CLASS_NAME = __CLASS__;
    const ID = 'id';
    const CATEGORY = Category::FK;
    const DATE = 'date';
    const AMOUNT = 'amount';

    protected $table=self::TABLE;
    public $timestamps=false;
    protected $fillable=array(
        self::DATE,
        self::AMOUNT
    );

    public function category(){
        return $this->belongsTo(
            Rol::CLASS_NAME,
            self::CATEGORY
        );
    }
}
