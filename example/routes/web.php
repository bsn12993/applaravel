<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

/**
 * User
 */
Route::resource('user','UserController');

/**
 * Admin
 */
Route::get('admin','FrontController@admin');
Route::get('home','FrontController@index');
Route::resource('log','LogController');
Route::get('unauthorized','LogController@unauthorized');
Route::get('logout','LogController@logout');

/**
 * Products
 */
Route::resource('product','ProductController');
Route::get('getProducts','ProductController@listing');
Route::post('createProduct','ProductController@storeProduct');
Route::put('updateProduct','ProductController@updateProduct');

/**
 * Roles
 */
Route::resource('rol','RolController');
Route::get('getRoles','RolController@listing');

/**
 * Files
 */
Route::resource('file','FileController');


/**
 * Mail
 */

Route::resource('mail','MailController');

 /**
  * Category
  */
Route::resource('category','CategoryController');
Route::get('listingCategories','CategoryController@listing');

/**
 * Expense
 */
Route::resource('expense','ExpenseController');

/**
 * Incomes
 */
Route::resource('income','IncomeController');


 
Route::post('api/create','ApiController@store')->middleware('accesskey');
