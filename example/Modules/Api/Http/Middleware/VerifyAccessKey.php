<?php

namespace Modules\Api\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class VerifyAccessKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $key=$request->headers->get('api-key');
        if(isset($key)==env('API_KEY'))
            return $next($request);
        else
            return response()->json(['error'=>'Acceso no autorizado'],401);
    }
}
