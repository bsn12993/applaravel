@extends('layouts.main')
@section('content')

    @if(Session::has('message-error'))
        <h3>{{Session::get('message-error')}}</h3>
    @endif

    @if(count($errors))
        <ol>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ol>
    @endif


    {!!Form::open(['route'=>'log.store', 'method'=>'POST'])!!}
        {!!Form::label('Correo:')!!}
        {!!Form::text('email',null,['class'=>'form-control'])!!}
        {!!Form::label('Contraseña:')!!}
        {!!Form::text('password',null,['class'=>'form-control'])!!}
        <button type="submit">Entrar</button>
    {!!Form::close()!!}

@stop