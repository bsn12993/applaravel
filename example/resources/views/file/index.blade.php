@extends('layouts.main')
@section('content')

    <div class="col-md-4"></div>
    <div class="col-md-8">
        <h3>Subir archivo</h3>

        {!!Form::open(['route'=>'file.store','method'=>'POST','files'=>true])!!}
            <div class="form-group">
                <input type="file" name='path' id='path'>
            </div>
            <button type="submit">Subir</button>
        {!!Form::close()!!}
    </div>
    <div class="col-md-4"></div>

@stop
