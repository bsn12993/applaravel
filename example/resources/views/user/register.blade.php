<div class="row">
        <div class="col-2"></div>
        <div class="col-md-8">
            <h1>Registrar usuario</h1>

            @if(count($errors)>0)
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{!!$error!!}</li>
                    @endforeach
                </ul>
            @endif

            {!!Form::open(['route'=>'user.store','method'=>'POST'])!!}
                <div class="form-group">
                    {!!Form::label('Nombre:')!!}
                    {!!Form::text('name',null,['class'=>'form-control','placeholder'=>'Ingresa el Nombre del usuario'])!!}
                </div>
                <div class="form-group">
                    {!!Form::label('Correo:')!!}
                    {!!Form::text('email',null,['class'=>'form-control','placeholder'=>'Ingresa el Correo del usuario'])!!}
                </div>
                <div class="form-group">
                    {!!Form::label('Contraseña:')!!}
                    {!!Form::text('password',null,['class'=>'form-control','placeholder'=>'Ingresa la Contraseña del usuario'])!!}
                </div>
                <button type="submit" class="btn btn-primary">Registrar</button>
            {!!Form::close()!!}
        </div>
        <div class="col-2"></div>
    </div>