@extends('layouts.main')
@section('content')

    <!-- Vertical Layout | With Floating Label -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Editar Gasto
                        <small></small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    {!!Form::model($expense,['route'=>['expense.update',$expense->id],'method'=>'PUT'])!!}
                        @if(count($errors)>0)
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{!!$error!!}</li>
                                @endforeach
                            </ul>
                        @endif
                        <div class="form-group form-float">
                            <div class="form-line">
                                {!!Form::text('date',null,['class'=>'form-control'])!!}
                                <label class="form-label">Fecha</label>
                                </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                {!!Form::text('amount',null,['class'=>'form-control'])!!}
                                <label class="form-label">Monto</label>
                            </div>
                        </div>

                        <div class='form-group form-float'>
                            <div class='form-line'>
                                <select name="categoryId" id="categoryId" class='form-control'>
                                    <option value="">-Selecciona-</option>
                                </select>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary m-t-15 waves-effect">Crear</button>
                    {!!Form::close()!!}
                </div>
            </div>
        </div>
    </div>
    <!-- Vertical Layout | With Floating Label -->
    <!-- Jquery Core Js -->
    <script src="../../plugins/jquery/jquery.min.js"></script>
    <script src="/js/global/global.js"></script>
    <script src="/js/category/category.js"></script>
@stop