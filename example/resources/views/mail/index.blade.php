@extends('layouts.main')
@section('content')


    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">

            @if(count($errors))
                @foreach($errors->all() as $error)
                    <span>{!!$error!!}</span><br>
                @endforeach
            @endif

            {!!Form::open(['route'=>'mail.store','method'=>'POST'])!!}
                <div class="form-group">
                    <label for="">Correo</label>
                    <input type="text" class="form-control" name="email" id="email">
                </div>
                <div class="form-group">
                    <label for="">Nombre</label>
                    <input type="text" class="form-control" name="name" id="name">
                </div>
                <div class="form-group">
                    <label for="">Mensaje</label>
                    <textarea name="message" id="message" cols="30" rows="10"></textarea>
                </div>
                <button type="submit">Enviar</button>
            {!!Form::close()!!}
        </div>
        <div class="col-md-4"></div>
    </div>
@stop