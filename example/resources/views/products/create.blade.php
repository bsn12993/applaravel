@extends('layouts.main')
@section('content')
    <h1>Crear Producto</h1>

    <div class='col-md-4'></div>
    <div class='col-md-4'>

        <div id="msg-error"></div>

        <a href="/product">Productos</a>

        <form id="form_createProduct">
            <div class='form-group'>
                {!!Form::label('Nombre:')!!}
                {!!Form::text('name',null,['id'=>'name','class'=>'form-control'])!!}
            </div>
            <div class='form-group'>
                {!!Form::label('Descrición:')!!}
                {!!Form::text('description',null,['id'=>'description','class'=>'form-control'])!!}
            </div>
            <div class='form-group'>
                {!!Form::label('Precio:')!!}
                {!!Form::text('price',null,['id'=>'price','class'=>'form-control'])!!}
            </div>
            <div class='form-group'>
                {!!Form::label('Estatus:')!!}
                <input type="checkbox" name="status" id="status" value="0", class="form-control"/>
            </div>
            {!!Form::submit('Crear',['class'=>'btn btn-primary'])!!}
        </form>
    </div>
    <div class='col-md-4'></div>
@stop