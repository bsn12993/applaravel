@extends('layouts.main')
@section('content')
<h1>Crear Producto</h1>

<div class='col-md-4'></div>
<div class='col-md-4'>

    <div id="msg-error"></div>

    <a href="/product">Productos</a>

    {!!Form::model($product,['route'=>['product.update',$product->id],'id' => 'formUpdateProduct','method'=>'PUT'])!!}
        <input type="hidden" id="idproduct" name="idproduct" value="{{$product->id}}"/>
        <div class='form-group'>
            {!!Form::label('Nombre:')!!}
            {!!Form::text('name',null,['id'=>'name','class'=>'form-control'])!!}
        </div>
        <div class='form-group'>
            {!!Form::label('Descrición:')!!}
            {!!Form::text('description',null,['id'=>'description','class'=>'form-control'])!!}
        </div>
        <div class='form-group'>
            {!!Form::label('Precio:')!!}
            {!!Form::text('price',null,['id'=>'price','class'=>'form-control'])!!}
        </div>
        {!!Form::submit('Crear',['class'=>'btn btn-primary'])!!}
    {!!Form::close()!!}
</div>
<div class='col-md-4'></div>
@stop