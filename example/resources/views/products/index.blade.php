@extends('layouts.main')
@section('content')

    {!!link_to_route('product.create',$title='Crear producto',null,$attributes=['class'=>'btn btn-primary'])!!}
    <table class="table">
        <thead>
            <th>Nombre</th>
            <th>Precio</th>
            <th>Editar</th>
            <th>Eliminar</th>
        </thead>
        <tbody id="tbl_product">
                
        </tbody>
    </table>

@stop

 

