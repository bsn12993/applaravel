@extends('layouts.main')
@section('content')

     <!-- Vertical Layout | With Floating Label -->
     <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        Rol
                        <small></small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    @if(Session::has('message'))
                        <h2>{!!Session::get('message')!!}</h2>
                    @endif

                    <a href="/rol/create">Crear rol</a>
                    <div class='table-responsive'>
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <td>Nombre</td>
                                    <td>Editar</td>
                                    <td>Eliminar</td>
                                </thead>
                                @foreach($roles as $rol)
                                    <tbody>
                                        <td>{{$rol->name}}</td>
                                        <td>{!!link_to_route('rol.edit',$title='Editar',$parameters=$rol->id,$attributes=['class'=>'btn btn-primary'])!!}</td>
                                        <td>
                                            {!!Form::open(['route'=>['rol.destroy',$rol->id],'method'=>'DELETE'])!!}
                                                <button type="submit" class="btn btn-danger">Eliminar</button>
                                            {!!Form::close()!!}
                                        </td>
                                    </tbody>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Vertical Layout | With Floating Label -->
    

@stop